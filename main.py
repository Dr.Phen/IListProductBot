from aiogram import executor
from bot import dp
from os import getcwd





if __name__ == "__main__":
    current_dir = getcwd()
    print(f"Current dir: {current_dir}")
    executor.start_polling(dp, skip_updates=True)

